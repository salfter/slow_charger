$fn=30;

translate([0,0,11.6])
    %import("slow_charger-v2.stl");

// translate([9.5,0,11.6])
//     %import("slow_charger-v1.stl");

difference()
{
    union()
    {
        translate([0,12.5,0])
        {
            difference()
            {
                union()
                {
                    cube([80,10,7]);
                    cube([10,10,10]);
                    translate([9.5,0,0])
                        cube([10,10,10]);
                    translate([70,0,0])
                        cube([10,10,10]);
                    translate([70-9.5,0,0])
                        cube([10,10,10]);
                }
                translate([4.445,4.5,0])
                {
                    cylinder(d=3.5, h=10);
                    cylinder(d=6.8, h=6, $fn=6);
                }
                translate([9.5+4.445,4.5,0])
                {
                    cylinder(d=3.5, h=10);
                    cylinder(d=6.8, h=6, $fn=6);
                }
                translate([80-4.445,4.5,0])
                {
                    cylinder(d=3.5, h=10);
                    cylinder(d=6.8, h=6, $fn=6);
                }
                translate([80-9.5-4.445,4.5,0])
                {
                    cylinder(d=3.5, h=10);
                    cylinder(d=6.8, h=6, $fn=6);
                }
            }
        }

        translate([0,71.2,0])
        {
            difference()
            {
                union()
                {
                    cube([80,10,7]);
                    cube([10,10,10]);
                    translate([9.5,0,0])
                        cube([10,10,10]);
                    translate([70,0,0])
                        cube([10,10,10]);
                    translate([70-9.5,0,0])
                        cube([10,10,10]);
                }
                translate([4.445,10-4.445,0])
                {
                    cylinder(d=3.5, h=10);
                    cylinder(d=6.8, h=6, $fn=6);
                }
                translate([4.445+9.5,10-4.445,0])
                {
                    cylinder(d=3.5, h=10);
                    cylinder(d=6.8, h=6, $fn=6);
                }
                translate([80-4.445,10-4.445,0])
                {
                    cylinder(d=3.5, h=10);
                    cylinder(d=6.8, h=6, $fn=6);
                }
                translate([80-9.5-4.445,10-4.445,0])
                {
                    cylinder(d=3.5, h=10);
                    cylinder(d=6.8, h=6, $fn=6);
                }
            }
        }

        translate([5,0,0])
            cube([70,4.5,7]);
    }

    for (x=[20:40:60])
    {
        for (y=[0:71.2:71.2])
        {
            translate([x,y,3.5])
            rotate([-90,0,0])
                cylinder(d=3.5, h=12);
        }
        translate([x,3,3.5])
        rotate([-90,0,0])
            cylinder(d=6.8, h=6, $fn=6);
    }
}

difference()
{
    union()
    {
        for (x=[0:75:75])
        translate([x,0,0])
        {
            cube([5,12.5,10]);
            translate([0,22.5,0])
                cube([5,71.2-22.5,10]);
        }
    }

    for (y=[7.5:57.5:65])
    {
        translate([0,y,5])
        rotate([0,90,0])
        {
            cylinder(d=3.5, h=80);
            translate([0,0,3])
            cylinder(d=6.8, h=6, $fn=6);
        }
    }
}

