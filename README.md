NiCd/NiMH Trickle Charger
=========================

This is a redesign of
[bigclivedotcom's simple charger](https://youtube.com/watch?v=wVnAH17f4jg).
It uses a USB-C charging port (tip for that comes from [the DigiKey
forum](https://forum.digikey.com/t/simple-way-to-use-usb-type-c-to-get-5v-at-up-to-3a-15w/7016))
and mostly SMD components, includes power headers to connect more boards
together, and provides an assembly-time option to configure for AA or AAA
cells.

